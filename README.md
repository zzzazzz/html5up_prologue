# html5up_prologue

## todo :
 - passer `|couper` en https://css-tricks.com/snippets/css/truncate-string-with-ellipsis/

----

## Les plugins

**<necessite>**

### Z-Core (zcore)

### Identité Extra (identite_extra)

### Compositions (compositions)
- Portfolio

### Pages Uniques (pages)

### Champs Extras (cextras)
Champs CSS pour attribuer une class FontAwesome à chaque article
Aller sur ?page=fontawesome pour avoir les refs.

----

**<utilise>**

### Porte-plume Partout (ppp)
Réglage : Activer la barre typographique sur **Chapeau**, et pour les autres, choisir : « Non »

### Mailcrypt (mailcrypt)

### Reseaux sociaux (sociaux)
