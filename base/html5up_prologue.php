<?php
/**
 * Déclarer le champ supplémentaire sur spip_articles
 *
 * @plugin     html5up_prologue
 * @copyright  2019
 * @author     Fa_b
 * @licence    GNU/GPL
 * @link       https://framagit.org/zzzazzz/html5up_prologue
 */

if (!defined("_ECRIRE_INC_VERSION")) return;
 
function html5up_prologue_declarer_champs_extras($champs = array()) {
  $champs['spip_articles']['css'] = array(
      'saisie' => 'input',//Type du champ (voir plugin Saisies)
      'options' => array(
            'nom' => 'css', 
            'label' => _T('html5up_prologue:css'), 
            'sql' => "varchar(255) NOT NULL DEFAULT ''",
            'defaut' => '',// Valeur par défaut
           // 'restrictions'=>array('voir' => array('auteur' => ''),//Tout le monde peut voir
           //             'modifier' => array('auteur' => 'webmestre')),//Seuls les webmestres peuvent modifier
      ),
  );
  return $champs;	
}
?>